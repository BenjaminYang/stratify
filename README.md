Git-Lab code 勘误

version MATLAB R2017a

tst_sca_l :		ylim([0 max(ext,[],'all')]);-->ylim([0 max(max(ext))]);

l-conv:		min(lam,[],'all');-->min(min(lam));  or  min(lam);   

        lam 是一维向量 两者似乎没有差别 （418，17.16）  （418，17.16）

tst_sca_fep:	 ylim([0 max(exblk,[],'all')]);-->ylim([0 max(exblk)]);

tst_nrg_dns： 注释掉xline(10,'-w','LineWidth',1); xline(40,'-w','LineWidth',1); xline(45,'-w','LineWidth',1);

tst_S: colormap redblue --> colormap pink

       MATLAB不支持平行符号，故将图例修改为
      legend('\Gamma_{\rm rad}^{\perp}', ...
       '\Gamma_{\rm rad}^{parallel}', ...
       '\Gamma_{\rm nrad}^{\perp}', ...
       '\Gamma_{\rm nrad}^{parallel}',...
       'Interpreter','latex');
       
tst_far_fld: legend('|S_\perp(\theta)|^2','|S_{parallel(\theta)}|^2','Interpreter','latex');

 --from Benjamin Yang

*******************************************************************************************************************************
**STRATIFY** is a comprehensive **MATLAB** package for calculating basic electromagnetic properties of a general stratified (multilayered) sphere. Its detailed description with underlying theory is freely available in [OSA Continuum](https://www.osapublishing.org/osac/abstract.cfm?uri=osac-3-8-2290)

The latest [release](https://gitlab.com/iliarasskazov/stratify/-/releases/v1.1) includes:

* absorption, scattering and extinction
* radiative and nonradiative decay rates of an electric dipole emitter located in any nonabsorbing shell, including host
* electric and magnetic near-fields in any shell, including host
* electric and magnetic energy density and total energy

The package contains the following folders with routines:

*    *bessel* for the Bessel and Riccati-Bessel functions
*    *decay* for spontaneous decay rates
*    *energy* for electromagnetic energy
*    *field* for near-fileds and far-field properties
*    *materials* for refractive indices 
*    *scripts* for examples of using the package
*    *util* for transfer matrices and electron free path correction

**References**

[1] [I. L. Rasskazov, P. S. Carney, and A. Moroz, "STRATIFY: a comprehensive and versatile MATLAB code for a multilayered sphere," OSA Continuum 3 (8), 2290-2306 (2020)](https://www.osapublishing.org/osac/abstract.cfm?uri=osac-3-8-2290)

[2] [A. Moroz, “A recursive transfer-matrix solution for a dipole radiating inside and outside a stratified sphere,” Annals Phys. 315, 352–418 (2005)](https://www.sciencedirect.com/science/article/pii/S0003491604001277)

[3] [I. L. Rasskazov, A. Moroz and P. S. Carney, “Electromagnetic energy in multilayered spherical particles,” J. Opt. Soc. Am. A 36 (9), 1591-1601 (2019)](https://www.osapublishing.org/josaa/abstract.cfm?uri=josaa-36-9-1591)

**Acknowledgements**

We thank Lorenzo Pattelli for useful comments and bug reports.
